<?php
/**
 * Table
 *
 * @package     Table
 * @author      Arsene Ntibushite
 * @copyright   Arsene  Ntibushite
 * @license     GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: Table
 * Version:     1.0.0
 * Author:      Arsene Ntibushite
 */




//[foobar]
function foobar_func( $atts ){

  $servername = "localhost";
  $database = "local";
  $username = "root";
  $password = "root";

  // Create connection
  //use wpdb instead of mysqli
  

  $conn = mysqli_connect($servername, $username, $password, $database);

  // Check connection

  if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
  }

  echo "Connected successfully";
  $res = $conn->query('SELECT * FROM wp_annuaire');
  $test = '<table class="center" style="margin-left: auto;
   margin-right: auto; border: 2px solid black ;  box-shadow: 0 0 20px rgba(0, 0, 0, 0.15); ">
     <tr>
     <th style="   background-color: #303030;color: #ffffff;
    text-align: left;">Id</th>
       <th style="   background-color: #303030;color: #ffffff;
    text-align: left;">Nom Entreprise</th>
       <th style="   background-color: #303030;
    color: #ffffff;
    text-align: left;">Localisation Entreprise</th>
       <th style="   background-color: #303030;
    color: #ffffff;
    text-align:    background-color: #303030;
    color: #ffffff;
    text-align: left;">Prenom Contact</th>
       <th style="   background-color: #303030;
    color: #ffffff;
    text-align: left;">Nom Contact</th>
       <th style="   background-color: #303030;
    color: #ffffff;
    text-align: left;">Mail Contact</th>
     </tr>';
    
     while($r= $res->fetch_array(MYSQLI_ASSOC)){
    
    $test = $test . '<tr>
       <td style="border: padding: 12px 15px;">'. $r["id"] .'</td>
       <td style="border: padding: 12px 15px;">' . $r["nom_entreprise"] .'</td>
       <td style="border: padding: 12px 15px;">'. $r["localisation_entreprise"] .'</td>
       <td style="border: padding: 12px 15px;">'.$r["prenom_contact"] .'</td>
       <td style="border: padding: 12px 15px;">'.$r["nom_contact"] .'</td>
       <td style="border: padding: 12px 15px;">'.$r["mail_contact"] .'</td>
     </tr>';

  };
  $test = $test .'</table>';
  return $test;

}
add_shortcode( 'foobar', 'foobar_func' );